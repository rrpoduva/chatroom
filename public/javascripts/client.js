var server_name = "http://192.168.0.21:3000/";
var socket = io.connect(server_name);

var currentUserName="";

socket.on('newConnection',function(data){
	$("#LoginContainer").show();
	$("#ListOfOnlineUsers").empty();
	console.log("Connected!");
	
	$("#ChatContainer").hide();
	$("#chatWindow").empty();
});

/*On Successful Submission of Username*/
socket.on('ConnectionSuccessful',function(data){
	console.log("Username created successfully");
	$("#LoginContainer").hide();
	$("#ChatContainer").show();
	
	currentUserName = data.userName;
	/*Allow Enter Key to send chat*/
	$('#btn-input').keypress(function(e){
	      if(e.keyCode==13)
	      $('#btn-chat').click();
	    });
});

/*Get List of Online Users*/

socket.on('onlineUsers',function(data){
	$("#ListOfOnlineUsers").empty();
	$("#ListOfOnlineUsers").show();
	
	$("#ListOfOnlineUsers").append("<lh style=\"align: center;\">List of Online Users</lh>");
	for(var i=0; i<data.userList.length;i++)
	{
		$("#ListOfOnlineUsers").append(
				"<li class=\"list-group-item\">"
						+ data.userList[i]
						+ "</li>");
	}
});

socket.on('newChat',function(data){
	var userName = data.userName;
	var chatMsg = data.chatMsg;
	
	var outputHTML = createIndividualChatUI(chatMsg,"Other",userName);
	$("#chatWindow").append(outputHTML);
});

socket.on('newUser',function(data){
	var userName = data.userName;
	var chatMsg = data.chatMsg;
	
	var outputHTML = createAdminChatMsgUI(chatMsg,userName);
	$("#chatWindow").append(outputHTML);
});

socket.on('userDisconnect',function(data){
	var userName = data.userName;
	var chatMsg = data.chatMsg;
	
	var outputHTML = createAdminChatMsgUI(chatMsg,userName);
	$("#chatWindow").append(outputHTML);
});

/*On Click of a submit button*/
function onClickSubmit(){
	currentUserName = document.getElementById("userName").value;
	socket.emit('userName',{
		userName: currentUserName
	});
	event.preventDefault(); 
	return false;
}
/*Hide Chat Window*/
$(document).ready(function(){ 
 
	$("#ChatContainer").hide();
	
});

function createAdminChatMsgUI(chatMsg,userName)
{
	var outputHTML = createIndividualChatUI((userName + " "+ chatMsg),"Other",
			"Moderator");
	
	/*Create a JqueryObj from String
	 * Searching will be easier*/
	
	var JqueryObj = $('<div/>').html(outputHTML).contents();
	/*Remove the circle for the Moderator*/
	JqueryObj.find("#userAvatar").remove();	
	JqueryObj.find("p").css("color","red");
	outputHTML = JqueryObj.html();
	return outputHTML;
}

function createIndividualChatUI(chatMsg,typeOfUser,userName)
{
	var htmlString="";
	
	if(typeOfUser=="Self")
		htmlString+="<li class=\"right clearfix\">";
	else
		htmlString+="<li class=\"left clearfix\">";
	
	if(typeOfUser=="Self")
		htmlString+="<span id=\"userAvatar\" class=\"chat-img pull-right\">";
	else if(typeOfUser=="Other")
		htmlString+="<span id=\"userAvatar\" class=\"chat-img pull-left\">";
	
	if(typeOfUser=="Self")
		htmlString+="<img src=\"http://placehold.it/50/FA6F57/fff&amp;text=ME\" alt=\"User Avatar\" class=\"img-circle\"/>";
	else if(typeOfUser=="Other")
		htmlString+="<img src=\"http://placehold.it/50/55C1E7/fff&amp;text=U\" alt=\"User Avatar\" class=\"img-circle\"/>";
	
	htmlString+="</span>";
	
	htmlString+="<div class=\"chat-body clearfix\">";
	
	htmlString+="<div class=\"header\">";
	
	if(typeOfUser=="Self")
		htmlString+="<strong class=\"pull-right primary-font\">";
	else if(typeOfUser=="Other")
		htmlString+="<strong class=\"primary-font\">";
	
	htmlString+= userName;
	
	htmlString+="</strong></div>";
	
	htmlString+="<p>" + chatMsg + "</p>";
	htmlString+="</div>";
	htmlString+="</li>";
	
	return htmlString;
}	


/*When user enters new chat*/
$("#btn-chat").click(function(){
	var chatMsg = document.getElementById("btn-input").value;
	socket.emit('newMsg',{
		userName: currentUserName,
		chatMsg: chatMsg
	});
	
	/*Update UI*/
	var outputHTML = createIndividualChatUI(chatMsg,"Self",currentUserName);
	$("#chatWindow").append(outputHTML);
	
	/*Scroll to bottom*/
	$('.panel-body').animate({
        scrollTop: $('.panel-body')[0].scrollHeight}, 2000);
	
	document.getElementById("btn-input").value = "";
})
