var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();


var server = require('http').createServer(app);
var port = 3000;
var io = require('socket.io').listen(server);

//Store List of properties of each connected user.
var list_of_users = {};
var count_of_users = 1;
server.listen(port);

function listOfOnlineUsers(onlineUsers)
{
	for(var keys in list_of_users)
	{
		if(list_of_users.hasOwnProperty(keys))
		{
			onlineUsers.push(keys);
		}
	}
	return onlineUsers;
}

io.sockets.on('connection', function(socket) {
	/*New Connection has been Made.
	 * Ask User to Enter UserName*/
	socket.emit('newConnection');
	
	/*Receive Username from the client*/
	socket.on('userName',function(data){
		
		socket.userName = data.userName + count_of_users;
		var userDetails ={userName: socket.userName};
				
		/*Append to list of users*/
		list_of_users[socket.userName]=userDetails;
		
		socket.emit('ConnectionSuccessful',{
			serverMsg: "UserCreated successfully",
			userName: socket.userName
		});
		
		/*Inform everyone on entry of new user*/
		socket.broadcast.emit('newUser',{
			userName: socket.userName,
			chatMsg: " has entered the Chat Room!"
		});
		
		/*Send List of Online users*/
		var onlineUsers=[];
		
		onlineUsers = listOfOnlineUsers(onlineUsers);
		
		socket.broadcast.emit('onlineUsers',{
			userList: onlineUsers
		});
		
		socket.emit('onlineUsers',{
			userList: onlineUsers
		});
		onlineUsers.length = 0;
		console.log("Username Created: " + socket.userName);
		count_of_users++;
		
	});
	
	/*A new chat msg has been sent by the client*/
	socket.on('newMsg',function(data){
		socket.broadcast.emit('newChat',{
			userName: data.userName,
			chatMsg: data.chatMsg
		});
	});
	
	/*A client has disconnected*/
	socket.on('disconnect',function(data){
		
		delete list_of_users[socket.userName];
		socket.broadcast.emit('userDisconnect',{
			userName: socket.userName,
			chatMsg: " has disconnected"
		});
		
		var onlineUsers=[];
		
		onlineUsers = listOfOnlineUsers(onlineUsers);
		
		socket.broadcast.emit('onlineUsers',{
			userList: onlineUsers
		});
		
		onlineUsers.length = 0;
	});
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});




module.exports = app;
